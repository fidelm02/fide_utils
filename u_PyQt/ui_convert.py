import os
import sys
import subprocess
import traceback
import time

def get_command(ui_path, output_path=None):
    cmd_        = '{EXEC_} "{INPUT_}" -o "{OUTPUT_}"'
    if not isinstance(ui_path, str):
        return None, None
    if not os.path.exists(ui_path):
        return None, None
    if not ui_path.endswith('.ui'):
        return None, None

    if output_path is not None:
        if not isinstance(output_path, str):
            output_path = None
        elif not os.path.exists(output_path):
            output_path = None
    
    pyuic_path  = "C:/Python27/Lib/site-packages/PyQt4/pyuic4.bat"
    output_file = get_new_path(ui_path, output_path)

    full_cmd = cmd_.format(EXEC_=pyuic_path,
                           INPUT_=ui_path,
                           OUTPUT_=output_file)
    full_cmd = full_cmd.replace('\\', '/')

    """
    second_cmd = '{PYTHON_EXEC} "{THIS_FILE}" "{NEW_FILE}"'

    second_cmd = second_cmd.format(PYTHON_EXEC='python',
                                   THIS_FILE=__file__,
                                   NEW_FILE=output_file)
    second_cmd = second_cmd.replace('\\', '/')
    """

    return full_cmd, output_file

def get_new_path(ui_path, output_path=None):
    if output_path is not None:
        file_name    = os.path.basename(ui_path)
        #output_name  = file_name.replace('.ui', '_converted.py')
        output_name  = file_name.replace('.ui', '.py')
        output_file  = os.path.join(output_path, output_name)
    else:
        #output_file = ui_path.replace('.ui', '_converted.py')
        output_file = ui_path.replace('.ui', '.py')
    
    return output_file

def run_command_subprocess(command_):
    try:
        process_    = subprocess.Popen(command_, shell=True, 
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
        err_, out_  = process_.communicate()
        if err_:
            return False
        else:
            return True
    except:
        print(traceback.format_exc())

def update_libraries(file_path):
    orig_line = 'from PyQt4 import QtCore, QtGui\n'
    neW_line  = 'from sgtk.platform.qt import QtCore, QtGui\n'

    # Using readlines()
    file1 = open(file_path, 'r')
    lines = file1.readlines()
    
    # Strips the newline character
    for index in range(len(lines)):
        if lines[index] == orig_line:
            lines[index] = neW_line
            break
    
    # Writing to a file
    file_ = open(file_path, 'w')
    file_.writelines(lines)
    file_.close()

def convert_file(file_path, output_dir=None):
    command_1, out_file = get_command(file_path, output_dir)
    if command_1 is not None:
        print('Converting UI to py')
        run_command_subprocess(command_1)
        print('Fixing qt shotgun libraries')
        update_libraries(out_file)
        print('End of process')

def convert_directory(dir_path, output_dir=None):
    for f in os.listdir(dir_path):
        convert_file(os.path.join(dir_path, f), output_dir)
        

if __name__ == "__main__":
    args = sys.argv
    python_file = None
    ui_file = False
    for arg in args:
        #if arg.endswith('_converted.py'):
        #   ""        
        if arg.endswith('.ui'):
            ui_file = True

    if not ui_file and len(args)>1:
        python_file = args[-2]

    if python_file is not None:
        update_libraries(python_file)
            
    else:
        print('Starting process')
        dir_       = "C:/Dev/Fidel/apps/mty-multi-templater/resources"
        output_dir = "C:/Dev/Fidel/apps/mty-multi-templater/python/template_system/ui"

        dir_       = "D:/Fidel/Documents/Trabajo/MrX/Development/XShare/UI"
        output_dir = dir_
        #convert_directory(dir_, output_dir)

        file_      = "D:/Fidel/Documents/Trabajo/FerozCarmesi/FZ_Apps/fc_app_ocio_converter/python/fc_ocio_converter/ui/ocio_converter_form.ui"
        #output_dir= "D:/Fidel/Documents/Trabajo/FerozCarmesi/FZ_Apps/fc_app_checksum/python/fc_checksum/ui"
        output_dir = os.path.dirname(file_)
        convert_file(file_, output_dir)
        


# Archivo = Modulo
# Importar modulos

# Documentation
#   https://developer.shotgunsoftware.com/python-api/reference.html#shotgun_api3.shotgun.Shotgun.connect

# Libraria/Modulo de sistema
import sys
sys.path.append("/location/python-api-master")

from shotgun_api3 import shotgun

url_  = "https://my_sg_site.shotgunstudio.com/"
user_ = "my_user@email.com"
pass_ = "my_password"

"""
o
script_name = "ScriptTest"
api_key     = "sgkmmpjualhgjD?nveh2hmjyl"
"""
#sg    = shotgun.Shotgun(url_, login=user_, password=pass_, connect=True)
sg    = shotgun.Shotgun(url_, script_name="ScriptTest", 
						api_key="sgkmmpjualhgjD?nveh2hmjyl", connect=True)


# 1.- Que es lo que estoy buscando (Entidad)
# 2.- Como lo diferencio de otros
# 3.- Que es lo que quiero de ahi

p1_entity  = 'Project'

# Filtros
# Filtro se divide en 
#   "Que info"  "Comparacion"  "Contra que"
#   "id" "is" 70
proj_id    = 70
p2_filters = [["id", "is", proj_id]]

p3_field   = ["sg_description"]

busqueda   = sg.find_one(p1_entity, p2_filters, p3_field)

# [{'type': 'Project', 'id': 70, 'sg_description': 'Info cotorron'}]
# sg.find       Devuelve toda una lista
# sg.find_one   Devuelve solo un elemento
print(busqueda)
#  {'type': 'Project', 'id': 70, 'sg_description': 'Info cotorron'}
# Diccionario
"""
{
	'type': 'Project', 
	'id': 70, 
	'sg_description': 'Info cotorron'
}
"""
print(busqueda["sg_description"])
busqueda["pilin"] = "Cochito"
print(busqueda)

# C.R.U.D.
"""
CREATE
R SEARCH
UPDATE
DELETE
"""
data = {"project":        {"type": "Project",  "id": proj_id},
     	"sg_sequence":    {"type": "Sequence", "id": 23},
     	"code":           "001_200",
     	'sg_status_list': "ip"
		}

# 1.- Que voy a crear(entity type)
# 2.- Que va a tener
new_shot = sg.create('Shot', data)
print(new_shot)

# Update
data = {'description': "Hola mundo"}
# 1.- Que (entity type)
# 2.- ID
# 3.- Info

sg.update(new_shot['type'], new_shot['id'], data)


#sg.delete("Shot", 1174)
#sg.delete("Shot", 1175)
#sg.delete("Shot", 1176)
sg.revive("Shot", 1175)


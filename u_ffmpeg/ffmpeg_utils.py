import os
import subprocess
import traceback

class ffmpeg_actions:

    def __validate_destination(self, destination):
        # Validating if the output folder exitst
        destination_folder = os.path.dirname(destination)
        if not os.path.exists(destination_folder):
            os.makedirs(destination_folder)

    def __execute_command(self, cmd_):
        transcode_images = subprocess.Popen(cmd_, startupinfo = subprocess.STARTUPINFO(),
                                            stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE,
                                            stdin=subprocess.PIPE)

        try:
            stdout, stderr = transcode_images.communicate()
            if transcode_images.returncode != 0:
                raise Exception("Failed to transcode images: %s" % (str(stderr) + '\n' + str(stdout) + '\n' + str(cmd_)))

        except Exception as e:
            print traceback.format_exc()

    def convert_sequence(self, initial_sequence, destination_sequence, extra_args=None):
        # Check destinarion
        self.__validate_destination(destination_sequence)

        convert_cmd = ['ffmpeg.exe']
        """
        ffmpeg -i input.avi -c:v prores -profile:v 3 -c:a pcm_s16le output.mov
        """
        if extra_args is not None:
            if 'first' in extra_args.keys():
                convert_cmd.append('-y')
                convert_cmd.append('-start_number')
                convert_cmd.append(str(extra_args['first']))
                convert_cmd += ["-vcodec", "libx264"]
                convert_cmd += ["-pix_fmt", "yuv422p"]
                convert_cmd += ["-r", "24"]

        convert_cmd.append('-i')
        convert_cmd.append('"{}"'.format(initial_sequence))
        convert_cmd.append('"{}"'.format(destination_sequence))

        convert_str = ' '.join(convert_cmd)
        self.__execute_command(convert_str)
        

    def convert_seq_to_video(self, source, video, extra_args=None):
        # Check destinarion
        self.__validate_destination(video)

        convert_cmd = ['ffmpeg.exe']
        if extra_args is not None:
            if 'first' in extra_args.keys():
                convert_cmd.append('-y')
                convert_cmd.append('-start_number')
                convert_cmd.append(str(extra_args['first']))

        convert_cmd.append('-i \"{}\"'.format(source))       
        #convert_cmd.append("-vcodec libx264")
        convert_cmd.append("-vcodec libx264rgb")
        convert_cmd.append("-pix_fmt yuv422p")
        convert_cmd.append("-r 24")
        convert_cmd.append("-map 0:v")
        convert_cmd.append("-crf 5")
        #convert_cmd.append('-vf "colorspace=bt709:iall=bt601-6-625:fast=1"')

        convert_cmd.append('\"{}\"'.format(video))

        convert_str = ' '.join(convert_cmd)
        self.__execute_command(convert_str)


def convert_sequence(source, destination, args=None):
    ffmpeg_ops = ffmpeg_actions()
    ffmpeg_ops.convert_sequence(source, destination, args)

def convert_to_video(source, video, args=None):
    ffmpeg_ops = ffmpeg_actions()
    ffmpeg_ops.convert_seq_to_video(source, video, args)


